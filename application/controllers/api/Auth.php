<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

require_once(APPPATH . '/libraries/jwt/src/BeforeValidException.php');
require_once(APPPATH . '/libraries/jwt/src/SignatureInvalidException.php');
require_once(APPPATH . '/libraries/jwt/src/ExpiredException.php');
require_once(APPPATH . '/libraries/jwt/src/JWT.php');

use \Firebase\JWT\JWT;
use Restserver\Libraries\REST_Controller;

class Auth extends REST_Controller
{
    private $secretkey    = "whtLOBPMaE5POikorEhUYEclFMtuGviwOiQTixLROPCzISx3UK";
    private $uniqueIdName = "uuid";
    private $time         = "time";

    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Credentials: *');
        if ("OPTIONS" === $_SERVER['REQUEST_METHOD'] ) {
            die();
        }
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->database();
        $this->load->model('m_user');
    }

    # ------ #
    function uuidv4()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff), // 32 bits for "time_low"
            mt_rand(0, 0xffff),  // 16 bits for "time_mid"
            mt_rand(0, 0x0fff) | 0x4000, // 16 bits for "time_hi_and_version", four most significant bits holds version number 4
            mt_rand(0, 0x3fff) | 0x8000, // 16 bits, 8 bits for "clk_seq_hi_res", // 8 bits for "clk_seq_low",          
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)  // 48 bits for "node"
        );
    }

    function createtokenwithcookietime()
    {
        $date               = new DateTime();
        $payload['iat']     = $date->getTimestamp(); //create waktu
        $payload['exp']     = $date->getTimestamp() + 60; //1 menit expire token
        $uuid               = $this->uuidv4();
        $token              = JWT::encode($payload, $this->secretkey);
        setcookie($this->uniqueIdName, $uuid);
        setcookie($this->time, $date->getTimestamp());
        $result = [
            'result'  => [
                'status'  => TRUE,
                'message' => 'Expire 1 minutes',
                'token'   => $token
            ]
        ];
        echo json_encode($result);
    }

    function tokengenerate_post()
    {
        $date               = new DateTime();
        $lastgeneratetoken  = $_COOKIE[$this->time];
        if (!$lastgeneratetoken) {
            $this->createtokenwithcookietime();
        } elseif (($date->getTimestamp() - $lastgeneratetoken) <= 60) {
            $result = [
                'result'  => [
                    'status'  => FALSE,
                    'message' => "Warning, you're already create the token for last 60s"
                ]
            ];
            echo json_encode($result);
        } else {
            $this->createtokenwithcookietime();
        }
    }
    # !------ #

    public function xlogin_post()
    {
        $username = $this->post('user');
        $password = $this->post('pass');
        if($username == '') {
            $this->response([
                'status'  => FALSE,
                'pesan' => "username tidak boleh kosong"
            ],REST_Controller::HTTP_BAD_REQUEST);
        } elseif ($password == '') {
            $this->response([
                'status'  => FALSE,
                'pesan' => "password tidak boleh kosong"
            ],REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $tmp_username = $this->m_user->cek_username($username)->row();
            $jmltmp_username = count((array) $tmp_username);
            if ($jmltmp_username == 0) {
                $this->response([
                    'status'  => FALSE,
                    'pesan' => "Maaf, akun tidak terdaftar"
                ],REST_Controller::HTTP_BAD_REQUEST);
            } else {
                $acc_username = $this->m_user->cek_akun($username, md5($password))->row();
                $jmlacc_username = count((array) $acc_username);
                if ($jmlacc_username > 0) {
                    $date               = new DateTime();
                    $payload['iat']     = $date->getTimestamp(); //create waktu
                    $payload['exp']     = $date->getTimestamp() + 60; //1 menit expire token
                    $token              = JWT::encode($payload, $this->secretkey);
                    $this->response([
                        'status'  => TRUE,
                        'pesan' => 'Expire 1 minutes',
                        'token'   => $token
                    ],REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'  => FALSE,
                        'pesan' => "username / password tidak sesuai"
                    ],REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }
    }
}