<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

require_once(APPPATH . '/libraries/jwt/src/BeforeValidException.php');
require_once(APPPATH . '/libraries/jwt/src/SignatureInvalidException.php');
require_once(APPPATH . '/libraries/jwt/src/ExpiredException.php');
require_once(APPPATH . '/libraries/jwt/src/JWT.php');

use \Firebase\JWT\JWT;
use Restserver\Libraries\REST_Controller;

class Barang extends REST_Controller
{ 
    private $secretkey    = "whtLOBPMaE5POikorEhUYEclFMtuGviwOiQTixLROPCzISx3UK";
    private $uniqueIdName = "uuid";
    private $time         = "time";

    public function __construct()
    {
        date_default_timezone_set("Asia/Bangkok");
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Credentials: *');
        if ("OPTIONS" === $_SERVER['REQUEST_METHOD'] ) {
            die();
        }
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->database();
        $this->load->model('m_sparepart');
    }

    # ------ #
    function uuidv4()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff), // 32 bits for "time_low"
            mt_rand(0, 0xffff),  // 16 bits for "time_mid"
            mt_rand(0, 0x0fff) | 0x4000, // 16 bits for "time_hi_and_version", four most significant bits holds version number 4
            mt_rand(0, 0x3fff) | 0x8000, // 16 bits, 8 bits for "clk_seq_hi_res", // 8 bits for "clk_seq_low",          
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)  // 48 bits for "node"
        );
    }

    function createtokenwithcookietime()
    {
        $date               = new DateTime();
        $payload['iat']     = $date->getTimestamp(); //create waktu
        $payload['exp']     = $date->getTimestamp() + 60; //1 menit expire token
        $uuid               = $this->uuidv4();
        $token              = JWT::encode($payload, $this->secretkey);
        setcookie($this->uniqueIdName, $uuid);
        setcookie($this->time, $date->getTimestamp());
        $result = [
            'result'  => [
                'status'  => TRUE,
                'message' => 'Expire 1 minutes',
                'token'   => $token
            ]
        ];
        echo json_encode($result);
    }

    function tokengenerate_post()
    {
        $date               = new DateTime();
        $lastgeneratetoken  = $_COOKIE[$this->time];
        if (!$lastgeneratetoken) {
            $this->createtokenwithcookietime();
        } elseif (($date->getTimestamp() - $lastgeneratetoken) <= 60) {
            $result = [
                'result'  => [
                    'status'  => FALSE,
                    'message' => "Warning, you're already create the token for last 60s"
                ]
            ];
            echo json_encode($result);
        } else {
            $this->createtokenwithcookietime();
        }
    }
    # !------ #

    public function auth_post()
    {
        $user = $this->post('user');
        $pass = $this->post('pass');
        if($user == '') {
            $this->response([
                'status'  => FALSE,
                'pesan' => "username tidak boleh kosong"
            ],REST_Controller::HTTP_BAD_REQUEST);
        } elseif ($pass == '') {
            $this->response([
                'status'  => FALSE,
                'pesan' => "password tidak boleh kosong"
            ],REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $date               = new DateTime();
            $payload['iat']     = $date->getTimestamp(); //create waktu
            $payload['exp']     = $date->getTimestamp() + 60; //1 menit expire token
            $uuid               = $this->uuidv4();
            $token              = JWT::encode($payload, $this->secretkey);
            setcookie($this->uniqueIdName, $uuid);
            setcookie($this->time, $date->getTimestamp());
            $this->response([
                'status'  => TRUE,
                'message' => 'Expire 1 minutes',
                'token'   => $token
            ],REST_Controller::HTTP_OK);
        }
    }

    public function getalldatabarang_get()
    {
        $jwt = $this->input->get_request_header('Authorization');
        if ($jwt == "") {
            $this->response([
                'status'  => FALSE,
                'pesan' => "Key token tidak boleh kosong"
            ],REST_Controller::HTTP_BAD_REQUEST);
        } else {
            try {
                $decode = JWT::decode($jwt, $this->secretkey, array('HS256'));
                if($decode) {
                    $page       = $this->get('page');
                    $length     = $this->get('length');
                    $cari       = $this->get('search');

                    /* if($page == ''){
                        $hal = 1;
                    } else {
                        $hal = $page - 1;
                    }
                    $ofs = ($page - 1) * $length; */

                    if($page == '' || !$page || $page == 0 || $page == 1){
                        $hal = 0;
                    } else {
                        $hal = ($page - 1);
                    }

                    $ofs = ($hal) * $length;

                    $totaldata  = $this->m_sparepart->alldatabrgspr($cari);
                    $rowcount   = ($totaldata / $length);
                    $sql        = $this->m_sparepart->alldatabrgsprlimofs($cari, $length, $ofs);
                    $numrows    = $rowcount;
                    
                    $this->response([
                        "status"          => TRUE,
                        "page"            => $hal,
                        "length"          => $length,
                        "datatotalcount"  => ceil($numrows),
                        "data"            => $sql
                    ],REST_Controller::HTTP_OK);
                }
            } catch (Exception $e) {
                $this->response([
                    'status'  => FALSE,
                    'pesan' => "The token has Expired / token is invalid , please generate new token"
                ],REST_Controller::HTTP_BAD_REQUEST);
            }
        }
       
    }

    public function savemasterbarang_post()
    {
        $jwt = $this->input->get_request_header('Authorization');
        $kode = $this->post('kode');
        $nama = $this->post('nama');
        $harga = $this->post('harga');
        $status = $this->post('status');
        if ($jwt == "") {
            $this->response([
                'status'  => FALSE,
                'pesan' => "Key token tidak boleh kosong"
            ],REST_Controller::HTTP_BAD_REQUEST);
        } else {
            try {
                $decode = JWT::decode($jwt, $this->secretkey, array('HS256'));
                if($decode) {
                    $data = [
                        'kode_sparepart' => $kode,
                        'nama_sparepart' => $nama,
                        'harga_sparepart' => $harga,
                        'status_barang' => $status,
                        'kode_distributor' => 'DST-000000001',
                        'input_time' => date("Y-m-d H:i:s")
                    ];
                    $this->m_sparepart->simpan_master_sparepart($data);
                    $this->response([
                        'status'  => TRUE,
                        'pesan' => "Sukses simpan data master barang"
                    ],REST_Controller::HTTP_OK);
                }
            } catch (Exception $e) {
                $this->response([
                    'status'  => FALSE,
                    'pesan' => "The token has Expired / token is invalid , please generate new token"
                ],REST_Controller::HTTP_BAD_REQUEST);
            }
       }
    }
    public function updatemasterbarang_put()
    {
        $jwt = $this->input->get_request_header('Authorization');
        $idx = $this->uri->segment(4);
        $kode = $this->put('kode');
        $nama = $this->put('nama');
        $harga = str_replace(".","",$this->put('harga'));
        $status = $this->put('status');
        if ($jwt == "") {
            $this->response([
                'status'  => FALSE,
                'pesan' => "Key token tidak boleh kosong"
            ],REST_Controller::HTTP_BAD_REQUEST);
        } else {
            try {
                $decode = JWT::decode($jwt, $this->secretkey, array('HS256'));
                if($decode) {
                    $data = [
                        'kode_sparepart' => $kode,
                        'nama_sparepart' => $nama,
                        'harga_sparepart' => $harga,
                        'status_barang' => $status,
                        'kode_distributor' => 'DST-000000001',
                        'last_update' => date("Y-m-d H:i:s")
                    ];
                    $this->m_sparepart->update_master_sparepart($data,$idx);
                    $this->response([
                        'status'  => TRUE,
                        'pesan' => "Sukses update data master barang"
                    ],REST_Controller::HTTP_OK);
                }
            } catch (Exception $e) {
                $this->response([
                    'status'  => FALSE,
                    'pesan' => "The token has Expired / token is invalid , please generate new token"
                ],REST_Controller::HTTP_BAD_REQUEST);
            }
       }
    }

    public function hapusmasterbarang_delete()
    {
        $jwt = $this->input->get_request_header('Authorization');
        $idx = $this->uri->segment(4);
       
        if ($jwt == "") {
            $this->response([
                'status'  => FALSE,
                'pesan' => "Key token tidak boleh kosong"
            ],REST_Controller::HTTP_BAD_REQUEST);
        } else {
            try {
                $decode = JWT::decode($jwt, $this->secretkey, array('HS256'));
                if($decode){
                    $this->m_sparepart->deletebarang($idx);
                    $this->response([
                        'status'  => TRUE,
                        'pesan' => "Sukses hapus data master barang"
                    ],REST_Controller::HTTP_OK);
                } 
            } catch (Exception $e){
                $this->response([
                    'status'  => FALSE,
                    'pesan' => "The token has Expired / token is invalid , please generate new token"
                ],REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function getbarangbyid_get()
    {
        $id = $this->get('id');
        $jwt = $this->input->get_request_header('Authorization');
        if ($jwt == "") {
            $this->response([
                'status'  => FALSE,
                'pesan' => "Key token tidak boleh kosong"
            ],REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $decode = JWT::decode($jwt, $this->secretkey, array('HS256'));
            if($decode) {
                $barang = $this->m_sparepart->getmaster_sparepartbyid($id)->row();
                $data = [
                    'kode_sparepart' => $barang->kode_sparepart,
                    'nama_sparepart' => $barang->nama_sparepart,
                    'harga_sparepart' => $barang->harga_sparepart,
                    'status_barang' => $barang->status_barang,
                    'idx_barang' => $barang->idx,
                ];
                $this->response([
                    'status'  => TRUE,
                    "data" => $data
                ],REST_Controller::HTTP_OK);
            }
        }
    }
}
