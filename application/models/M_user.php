<?php

class M_user extends CI_Model
{
    public function cek_username($username)
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('username', $username);
        return $this->db->get();
    }

    public function cek_akun($username, $password)
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        return $this->db->get();
    }

    public function simpan_akun($data)
    {
        $this->db->insert('tb_user', $data);
        return true;
    }
}
