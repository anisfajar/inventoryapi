<?php

class M_sparepart extends CI_Model
{
    public function simpan_master_sparepart($data)
    {
        $this->db->insert('master_sparepart', $data);
        return true;
    }
    public function getmaster_sparepartbyid($id)
    {
        $this->db->select('*');
        $this->db->from('master_sparepart');
        $this->db->where('idx', $id);
        $this->db->order_by('idx', 'DESC');

        return $this->db->get();
    }

    public function update_master_sparepart($data, $id)
    {
        return $this->db->update('master_sparepart', $data, array('idx' => $id));
    }

    //rest API
    public function deletebarang($id)
    {
        $this->db->delete('master_sparepart', array('idx' => $id));
    }

    //serverside model lain
    public function alldatabrgspr($cari = "")
    {
        $this->db->select('*');
        $this->db->from('master_sparepart');
        if($cari != ""){
            $this->db->where('kode_sparepart',$cari);
        }
        $this->db->where('idx >',0);
        return $this->db->count_all_results();
    }

    public function alldatabrgsprlimofs($cari, $lmt, $ofs)
    {
        $this->db->select('*');
        $this->db->from('master_sparepart');
        if($cari != ""){
            $this->db->where('kode_sparepart',$cari);
            $this->db->limit($lmt,0);
        } else {
            $this->db->limit($lmt,$ofs);
        }
        return $this->db->get()->result();
    }
}
