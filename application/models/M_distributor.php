<?php

class M_distributor extends CI_Model
{
    var $column_search = ['nama_distributor'];
    public function getall_data()
    {
        $this->db->select('*');
        $this->db->from('distributor');
        return $this->db->get();
    }

    public function count_data()
    {
        $this->db->select("COUNT(*) as jml");
        $this->db->from("distributor");

        return $this->db->get();
    }

    public function get_datatables_distributor()
    {
        $this->db->select('*');
        $this->db->from('distributor');
        $this->db->order_by('idx', 'ASC');

        if (!empty($_POST["search"]['value'])) {
            $i = 0;
            foreach ($this->column_search as $item) {
                if ($_POST['search']['value']) {
                    if ($i === 0) {
                        $this->db->group_start()
                            ->like('lower(' . $item . ')', strtolower(trim($_POST['search']['value'])));
                    } else {
                        $this->db->or_like('lower(' . $item . ')', strtolower(trim($_POST['search']['value'])));
                    }

                    if (count($this->column_search) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }

        if (isset($_POST["length"]) && $_POST["length"] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_distributor()
    {
        $this->db->select('*');
        $this->db->from('distributor');
        $this->db->order_by('idx', 'ASC');
        return $this->db->count_all_results();
    }

    function count_filtered_distributor()
    {
        $this->db->select('*');
        $this->db->from('distributor');
        $this->db->order_by('idx', 'ASC');

        if (!empty($_POST["search"]['value'])) {
            $i = 0;
            foreach ($this->column_search as $item) {
                if ($_POST['search']['value']) {
                    if ($i === 0) {
                        $this->db->group_start()
                            ->like('lower(' . $item . ')', strtolower(trim($_POST['search']['value'])));
                    } else {
                        $this->db->or_like('lower(' . $item . ')', strtolower(trim($_POST['search']['value'])));
                    }

                    if (count($this->column_search) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }

        return $this->db->count_all_results();
    }

    public function simpan_distributor($data)
    {
        $this->db->insert('distributor', $data);
        return true;
    }
    public function getdistributorbyid($id)
    {
        $this->db->select('*');
        $this->db->from('distributor');
        $this->db->where('idx', $id);
        $this->db->order_by('idx', 'DESC');

        return $this->db->get();
    }

    public function update_distributor($data, $id)
    {
        return $this->db->update('distributor', $data, array('idx' => $id));
    }

    public function lastnumber()
    {
        $this->db->select('max(id_distributor) as lastkode');
        $this->db->from('distributor');
        return $this->db->get();
    }

    public function getdetaildistbykode($kode)
    {
        $hsl = $this->db->query("SELECT * FROM distributor WHERE id_distributor='$kode'");
        if ($hsl->num_rows() > 0) {
            foreach ($hsl->result() as $data) {
                $hasil = array(
                    'id_distributor' => $data->id_distributor,
                    'nama_distributor' => $data->nama_distributor,
                );
            }
        }
        return $hasil;
    }
}
